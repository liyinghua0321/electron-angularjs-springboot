const {
    app,
    BrowserWindow
} = require('electron');

var spawn = require('child_process').spawn
var free = spawn('java', ['-jar', `${__dirname}/war/wire.war`]);

let win;

function createWindow() {


    win = new BrowserWindow({
        width: 1200,
        height: 700
    });

    win.setMenu(null);

    win.loadFile(`${__dirname}/html/loading/loading.html`);

    setTimeout(() => {
        win.loadFile(`${__dirname}/index.html`);
    }, 3000);

    //控制台
    // win.webContents.openDevTools();

    win.on('close', () => {
        win = null;
    });

}

app.on('ready', createWindow);

app.on('window-all-closed', () => {

    // 注册子进程关闭事件
    free.on('exit', function (code, signal) {

    });

    if (process.platform !== 'darwin') {
        app.quit();
    }

});

//os x
app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});