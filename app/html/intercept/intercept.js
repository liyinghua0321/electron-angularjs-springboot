index.directive('autoHeight', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return {
                'h': $window.innerHeight,
            };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.style = function () {
                return {
                    'height': (newValue.h - 160) + 'px',
                };
            };

        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
})

index.controller('interceptController', ['$scope', '$http', '$stateParams', '$interval', function ($scope, $http, $stateParams) {

    $scope.device = $stateParams.device;
    $scope.insertKey = "false";
    $scope.texts = [];
    setInterval(function () {

        $http({
            method: 'post',
            url: 'http://localhost:9527/intercept',
            dataType: 'jsonp',
            withCredentials: true,
            params: {
                insert: $scope.insertKey
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(function (response) {
            if (response.data.state == "null") {
                return;
            }
            $scope.texts.push(response.data.state);
        }, function errorCallback(response) {
            console.log('失败');
        });
    }, 1000);

    $scope.setFilter = function () {

        if ($scope.filter == null) {
            alert("null");
            return;
        }
        $http({
            method: 'post',
            url: 'http://localhost:9527/setFilter',
            dataType: 'jsonp',
            withCredentials: true,
            params: {
                filter: $scope.filter
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(function (response) {
            $scope.getFilter = "----------" + response.data.state;
        }, function errorCallback(response) {
            console.log('失败');
        });
    }

    $scope.check = function () {

        if ($scope.checked) {
            $scope.insertKey = "true";
            $http({
                method: 'post',
                url: 'http://localhost:9527/createFile',
                dataType: 'jsonp',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).then(function (response) {

            }, function errorCallback(response) {
                console.log('失败');
            });
        } else {
            $scope.insertKey = "false";
        }

    }

    $scope.clear = function () {
        $scope.texts = [];
    }

}]);